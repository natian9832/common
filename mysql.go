package common

import "github.com/micro/go-micro/v2/config"

type MysqlConfig struct {
	Host     string `json:"host"`
	User     string `json:"user"`
	Pwd      string `json:"pwd"`
	Database string `json:"database"`
	Port     int64  `json:"port"`
}

// GetMysqlFromConsul 获取mysql的配置
func GetMysqlFromConsul(c config.Config, path ...string) (mysqlConfig *MysqlConfig) {
	mysqlConfig = &MysqlConfig{}
	_ = c.Get(path...).Scan(mysqlConfig)
	return
}
